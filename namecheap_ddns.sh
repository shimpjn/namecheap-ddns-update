#!/bin/sh

set +e

readonly HOST="$1"
readonly DOMAIN_NAME="$2"

if [ -z "$HOST" ]; then
    printf "No host provided.\n"
    exit 2
fi

if [ -z "$DOMAIN_NAME" ]; then
    printf "No domain provided.\n"
    exit 2
fi

read -r PASSWORD
if [ $? -ne 0 ]; then
    printf "No password provided.\n"
    exit 2
fi

curl --data "host=$HOST" \
     --data "domain=$DOMAIN_NAME" \
     --data "password=$PASSWORD" \
     'https://dynamicdns.park-your-domain.com/update'
